<?php

namespace Radavel;

use Doctrine\Instantiator\Exception\InvalidArgumentException;

class User {
    private $name;
    
    private $lastname;
    
    private $email;
    
    private $password;
    
    public function __construct($email, $password)
    {
        if (! filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            throw new InvalidArgumentException("El email [$email] no es valido");
        }
        
        $this->email = $email;
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }
    
    public function setName($name, $lastname)
    {
        $this->name = $name;
        $this->lastname = $lastname;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function changePassword($password)
    {
        if (! password_verify($password, $this->password))
        {
            throw new \InvalidArgumentException("The password is invalid");
        }
        
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

}