<?php

namespace Radavel;

class Product {
    private $name;
    
    private $identifier;
    
    private $description;
    
    private $stock;
    
    public function __construct($name, $identifier)
    {
        $this->name = $name;
        $this->identifier = $identifier;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
}