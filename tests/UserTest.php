<?php

class UserTest extends PHPUnit_Framework_TestCase
{
    /** @test */
    function it_should_be_able_to_construct()
    {
        $user = new \Radavel\User('rada@gruporlh.com', '123456');

        $this->assertInstanceOf(\Radavel\User::class, $user);
    }

    /** @test */
    function it_should_be_a_invalid_email()
    {
        $this->setExpectedException(
            \InvalidArgumentException::class
        );

        $user = new \Radavel\User('esto no es un email', '123456');
    }

    /** @test */
    function it_should_be_equals_password()
    {
        $user = new \Radavel\User('rada@gruporlh.com', '123456');

        $user->changePassword('123456');

        $this->assertInstanceOf(\Radavel\User::class, $user);
    }

    /** @test */
    function it_shold_fail_why_the_password_is_invalid()
    {
        $user = new \Radavel\User('email@gruporlh.com', '123456');

        $this->setExpectedException(
            \InvalidArgumentException::class
        );

        $user->changePassword('1234565');
    }
    
}