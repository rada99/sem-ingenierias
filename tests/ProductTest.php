<?php

class ProductTest extends PHPUnit_Framework_TestCase {
    /** @test */
    function it_should_be_an_instance()
    {
        $product = new Radavel\Product('Linux', 'Lin');

        $this->assertInstanceOf(\Radavel\Product::class, $product);
    }

    /** @test */
    function it_should_set_a_description()
    {
        $product = new \Radavel\Product('Linux', 'Lin');

        $product->setDescription('Es un software libre');

        $description = $product->getDescription();

        $this->assertEquals('Es un software libre', $description);
    }
}